# Zecwm
Big Daddy Zec's dwm fork
## Patches
+ attachbottom
+ fibonacci
+ urgentborder
+ restartsig
+ autostart (modified to look for (dwmlauncher.sh in .config: ~/.config/dwmlauncher.sh)

## Features
several custom scripts are used in the hotkeys section of config.h and and all of them and more can be found in my deskdots repo

## Installation
```
git clone https://gitlab.com/xZecora/zecs-dwm-fork.git
cd zecwm
sudo make install
```
And then store this source code somewhere nice and safe
